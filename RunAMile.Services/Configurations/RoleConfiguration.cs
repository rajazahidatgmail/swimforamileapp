﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunAMile.Services.Models;

namespace RunAMile.Services.Configurations
{
    
public class RoleConfiguration : EntityTypeConfiguration<tblRole>
{
    public RoleConfiguration()
    {
        ToTable("tbl_role");
        HasKey(t => t.ID);

        Property(t => t.ID)
            .HasColumnName("role_id")
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

        Property(t => t.NAME)
            .HasColumnName("role_name")
            .HasMaxLength(256)
            .IsRequired();
    }
}}
