﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunAMile.Services.Models;

namespace RunAMile.Services.Configurations
{
    
public class UserConfiguration : EntityTypeConfiguration<tblUser>
{
    public UserConfiguration()
    {
        ToTable("tbl_user_permitted");
        HasKey(t => t.ID);

        Property(t => t.ID)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
            .HasColumnName("id");

        Property(t => t.FIRST_NAME)
            .HasColumnName("first_name")
            .HasMaxLength(100)
            .IsRequired();

        Property(t => t.SURNAME)
            .HasColumnName("last_name")
            .HasMaxLength(100)
            .IsRequired();

        Property(t => t.USER_NAME)
            .HasColumnName("user_name")
            .HasMaxLength(100)
            .IsRequired();

        Property(t => t.PASSWORD)
            .HasColumnName("password");


        HasMany(t => t.tblRoles)
            .WithMany(t => t.tblUsers)
            .Map(m =>
            {
                m.ToTable("tbl_user_role");
                m.MapLeftKey("user_id");
                m.MapRightKey("role_id");
            });

    }
}}
