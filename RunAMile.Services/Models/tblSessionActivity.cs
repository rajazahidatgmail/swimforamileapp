using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblSessionActivity
    {
        public tblSessionActivity()
        {
            this.tblSessionActivityDetails = new List<tblSessionActivityDetail>();
        }

        public int Id { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> LaneNo { get; set; }
        public Nullable<System.Guid> GivenPassword { get; set; }
        public Nullable<System.DateTime> Date_Time { get; set; }

        public string ConductedByName { get; set; }

        public virtual tblSession tblSession { get; set; }
        public virtual ICollection<tblSessionActivityDetail> tblSessionActivityDetails { get; set; }
    }
}
