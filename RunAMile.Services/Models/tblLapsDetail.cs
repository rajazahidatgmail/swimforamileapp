using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblLapsDetail
    {
        public int Id { get; set; }
        public Nullable<int> SessionActivityDetailId { get; set; }
        public Nullable<System.DateTime> LapStartTime { get; set; }
        public Nullable<System.DateTime> LapFinishTime { get; set; }
        public Nullable<int> LapNo { get; set; }
        public Nullable<decimal> LapCompletionTime { get; set; }
        public virtual tblSessionActivityDetail tblSessionActivityDetail { get; set; }
    }
}
