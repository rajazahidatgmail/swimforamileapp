using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblRole
    {
        public tblRole()
        {
            this.tblUsers = new List<tblUser>();
        }

        public int ID { get; set; }
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<tblUser> tblUsers { get; set; }
    }
}
