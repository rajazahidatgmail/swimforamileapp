using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using RunAMile.Services.Models.Mapping;

namespace RunAMile.Services.Models
{
    public partial class RunAMileContext : DbContext
    {
        static RunAMileContext()
        {
            Database.SetInitializer<RunAMileContext>(null);
        }

        public RunAMileContext()
            : base("Name=RunAMileContext")
        {
        }

        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<tblEvent> tblEvents { get; set; }
        public DbSet<tblEventType> tblEventTypes { get; set; }
        public DbSet<tblLapsDetail> tblLapsDetails { get; set; }
        public DbSet<tblParticipant> tblParticipants { get; set; }
        public DbSet<tblRole> tblRoles { get; set; }
        public DbSet<tblSession> tblSessions { get; set; }
        public DbSet<tblSessionActivity> tblSessionActivities { get; set; }
        public DbSet<tblSessionActivityDetail> tblSessionActivityDetails { get; set; }
        public DbSet<tblUser> tblUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new tblEventMap());
            modelBuilder.Configurations.Add(new tblEventTypeMap());
            modelBuilder.Configurations.Add(new tblLapsDetailMap());
            modelBuilder.Configurations.Add(new tblParticipantMap());
            modelBuilder.Configurations.Add(new tblRoleMap());
            modelBuilder.Configurations.Add(new tblSessionMap());
            modelBuilder.Configurations.Add(new tblSessionActivityMap());
            modelBuilder.Configurations.Add(new tblSessionActivityDetailMap());
            modelBuilder.Configurations.Add(new tblUserMap());
        }
    }
}
