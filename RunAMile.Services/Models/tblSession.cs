using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblSession
    {
        public tblSession()
        {
            this.tblParticipants = new List<tblParticipant>();
            this.tblSessionActivities = new List<tblSessionActivity>();
        }

        public int Id { get; set; }
        public Nullable<int> EventId { get; set; }
        public int Heat { get; set; }
        public Nullable<System.DateTime> TimeFrom { get; set; }
        public Nullable<System.DateTime> TimeTo { get; set; }
        public int Lanes { get; set; }
        public string PasswordKey { get; set; }
        public virtual tblEvent tblEvent { get; set; }
        public virtual ICollection<tblParticipant> tblParticipants { get; set; }
        public virtual ICollection<tblSessionActivity> tblSessionActivities { get; set; }
    }
}
