using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblParticipant
    {
        public tblParticipant()
        {
            this.tblSessionActivityDetails = new List<tblSessionActivityDetail>();
        }

        public int Id { get; set; }
        public string BIB { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Age { get; set; }
        public string Email { get; set; }
        public string Division { get; set; }
        public Nullable<int> Lane { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> SrNo { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        
        public virtual ICollection<tblSessionActivityDetail> tblSessionActivityDetails { get; set; }
        public virtual tblSession tblSession { get; set; }
    }
}
