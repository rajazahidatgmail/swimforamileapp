using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblSessionActivityDetail
    {
        public tblSessionActivityDetail()
        {
            this.tblLapsDetails = new List<tblLapsDetail>();
        }

        public int Id { get; set; }
        public Nullable<int> SessionActivityId { get; set; }
        public Nullable<int> ParticipantId { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> FinishTime { get; set; }
        public string Status { get; set; }
        public virtual ICollection<tblLapsDetail> tblLapsDetails { get; set; }
        public virtual tblParticipant tblParticipant { get; set; }
        public virtual tblSessionActivity tblSessionActivity { get; set; }
    }
}
