using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblEventType
    {
        public tblEventType()
        {
            this.tblEvents = new List<tblEvent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalLength { get; set; }
        public int PoolLength { get; set; }
        public int Laps { get; set; }
        public virtual ICollection<tblEvent> tblEvents { get; set; }
    }
}
