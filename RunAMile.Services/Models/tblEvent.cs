using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblEvent
    {
        public tblEvent()
        {
            this.tblSessions = new List<tblSession>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> TotalLength { get; set; }
        public Nullable<int> PoolLength { get; set; }
        public Nullable<int> Laps { get; set; }
        public Nullable<int> Lanes { get; set; }
        public string PasswordKey { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public Nullable<int> EventTypeId { get; set; }
        public virtual tblEventType tblEventType { get; set; }
        public virtual ICollection<tblSession> tblSessions { get; set; }
    }
}
