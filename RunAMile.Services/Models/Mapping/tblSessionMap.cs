using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblSessionMap : EntityTypeConfiguration<tblSession>
    {
        public tblSessionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tblSession");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EventId).HasColumnName("EventId");
            this.Property(t => t.Heat).HasColumnName("Heat");
            this.Property(t => t.TimeFrom).HasColumnName("TimeFrom");
            this.Property(t => t.TimeTo).HasColumnName("TimeTo");
            this.Property(t => t.Lanes).HasColumnName("Lanes");
            this.Property(t => t.PasswordKey).HasColumnName("PasswordKey");

            // Relationships
            this.HasOptional(t => t.tblEvent)
                .WithMany(t => t.tblSessions)
                .HasForeignKey(d => d.EventId);

        }
    }
}
