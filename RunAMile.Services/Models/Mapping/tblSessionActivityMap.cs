using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblSessionActivityMap : EntityTypeConfiguration<tblSessionActivity>
    {
        public tblSessionActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tblSessionActivity");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.LaneNo).HasColumnName("LaneNo");
            this.Property(t => t.GivenPassword).HasColumnName("GivenPassword");
            this.Property(t => t.Date_Time).HasColumnName("Date_Time");
            this.Property(t => t.ConductedByName).HasColumnName("ConductedByName");
            

            // Relationships
            this.HasOptional(t => t.tblSession)
                .WithMany(t => t.tblSessionActivities)
                .HasForeignKey(d => d.SessionId);

        }
    }
}
