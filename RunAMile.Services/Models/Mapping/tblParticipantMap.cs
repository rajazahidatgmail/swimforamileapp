using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblParticipantMap : EntityTypeConfiguration<tblParticipant>
    {
        public tblParticipantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.BIB)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.FName)
                .HasMaxLength(50);

            this.Property(t => t.LName)
                .HasMaxLength(50);

            this.Property(t => t.Gender)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Division)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tblParticipants");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.BIB).HasColumnName("BIB");
            this.Property(t => t.FName).HasColumnName("FName");
            this.Property(t => t.LName).HasColumnName("LName");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Division).HasColumnName("Division");
            this.Property(t => t.Lane).HasColumnName("Lane");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.SrNo).HasColumnName("SrNo");
            this.Property(t => t.StartTime).HasColumnName("StartTime");

            // Relationships
            this.HasOptional(t => t.tblSession)
                .WithMany(t => t.tblParticipants)
                .HasForeignKey(d => d.SessionId);

        }
    }
}
