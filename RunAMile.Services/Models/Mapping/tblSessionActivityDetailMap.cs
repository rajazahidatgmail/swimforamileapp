using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblSessionActivityDetailMap : EntityTypeConfiguration<tblSessionActivityDetail>
    {
        public tblSessionActivityDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tblSessionActivityDetail");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SessionActivityId).HasColumnName("SessionActivityId");
            this.Property(t => t.ParticipantId).HasColumnName("ParticipantId");
            this.Property(t => t.StartTime).HasColumnName("StartTime");
            this.Property(t => t.FinishTime).HasColumnName("FinishTime");
            this.Property(t => t.Status).HasColumnName("Status");

            // Relationships
            this.HasOptional(t => t.tblParticipant)
                .WithMany(t => t.tblSessionActivityDetails)
                .HasForeignKey(d => d.ParticipantId);
            this.HasOptional(t => t.tblSessionActivity)
                .WithMany(t => t.tblSessionActivityDetails)
                .HasForeignKey(d => d.SessionActivityId);

        }
    }
}
