using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblRoleMap : EntityTypeConfiguration<tblRole>
    {
        public tblRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NAME)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DESCRIPTION)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tblRoles");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NAME).HasColumnName("NAME");
            this.Property(t => t.DESCRIPTION).HasColumnName("DESCRIPTION");

            // Relationships
            this.HasMany(t => t.tblUsers)
                .WithMany(t => t.tblRoles)
                .Map(m =>
                    {
                        m.ToTable("tblUserRoles");
                        m.MapLeftKey("TBL_REF_ROLES_ID");
                        m.MapRightKey("TBL_PERMITTED_USERS_ID");
                    });


        }
    }
}
