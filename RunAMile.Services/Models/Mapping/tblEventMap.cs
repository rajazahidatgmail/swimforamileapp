using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblEventMap : EntityTypeConfiguration<tblEvent>
    {
        public tblEventMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("tblEvent");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.TotalLength).HasColumnName("TotalLength");
            this.Property(t => t.PoolLength).HasColumnName("PoolLength");
            this.Property(t => t.Laps).HasColumnName("Laps");
            this.Property(t => t.Lanes).HasColumnName("Lanes");
            this.Property(t => t.PasswordKey).HasColumnName("PasswordKey");
            this.Property(t => t.EventDate).HasColumnName("EventDate");
            this.Property(t => t.EventTypeId).HasColumnName("EventTypeId");

            // Relationships
            this.HasOptional(t => t.tblEventType)
                .WithMany(t => t.tblEvents)
                .HasForeignKey(d => d.EventTypeId);

        }
    }
}
