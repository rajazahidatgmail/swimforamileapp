using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblEventTypeMap : EntityTypeConfiguration<tblEventType>
    {
        public tblEventTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tblEventTypes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.TotalLength).HasColumnName("TotalLength");
            this.Property(t => t.PoolLength).HasColumnName("PoolLength");
            this.Property(t => t.Laps).HasColumnName("Laps");
        }
    }
}
