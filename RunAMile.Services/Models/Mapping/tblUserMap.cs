using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblUserMap : EntityTypeConfiguration<tblUser>
    {
        public tblUserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.USER_NAME)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.FIRST_NAME)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SURNAME)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PASSWORD)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tblUsers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.USER_NAME).HasColumnName("USER_NAME");
            this.Property(t => t.FIRST_NAME).HasColumnName("FIRST_NAME");
            this.Property(t => t.SURNAME).HasColumnName("SURNAME");
            this.Property(t => t.PASSWORD).HasColumnName("PASSWORD");
        }
    }
}
