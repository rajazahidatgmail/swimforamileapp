using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace RunAMile.Services.Models.Mapping
{
    public class tblLapsDetailMap : EntityTypeConfiguration<tblLapsDetail>
    {
        public tblLapsDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("tblLapsDetail");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SessionActivityDetailId).HasColumnName("SessionActivityDetailId");
            this.Property(t => t.LapStartTime).HasColumnName("LapStartTime");
            this.Property(t => t.LapFinishTime).HasColumnName("LapFinishTime");
            this.Property(t => t.LapNo).HasColumnName("LapNo");
            this.Property(t => t.LapCompletionTime).HasColumnName("LapCompletionTime").HasPrecision(18, 2);

            // Relationships
            this.HasOptional(t => t.tblSessionActivityDetail)
                .WithMany(t => t.tblLapsDetails)
                .HasForeignKey(d => d.SessionActivityDetailId);

        }
    }
}
