using System;
using System.Collections.Generic;

namespace RunAMile.Services.Models
{
    public partial class tblUser
    {
        public tblUser()
        {
            this.tblRoles = new List<tblRole>();
        }

        public int ID { get; set; }
        public string USER_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string SURNAME { get; set; }
        public string PASSWORD { get; set; }
        public virtual ICollection<tblRole> tblRoles { get; set; }
    }
}
