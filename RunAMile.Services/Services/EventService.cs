﻿using log4net;
using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunAMile.Services
{
    public class EventService
    {
        public readonly ILog _log;
        private RunAMileContext _db;
        public EventService(RunAMileContext db)
            : base()
        {
            _log = LogManager.GetLogger(this.GetType());
            _db = db;
            _log.Debug("EventService constructor");
        }

        public IQueryable<tblEvent> GetEvent(string name, Int32? id)
        {
            return (from a in _db.tblEvents
                    where (name == null || name == "" || a.Name.Contains(name))
                    where (id.HasValue == false || id.Value < 1 || id == a.Id)
                    orderby name
                    select a);
        }
        public IQueryable<tblSession> GetSesions(Int32? EventId, Int32? id)
        {
            return (from a in _db.tblSessions
                    where (EventId.HasValue == false || EventId.Value < 1 || EventId == a.EventId)
                    where (id.HasValue == false || id.Value < 1 || id == a.Id)
                    orderby a.Heat
                    select a);
        }

        public IQueryable<tblEventType> GetEventType(string EventName)
        {
            return (from a in _db.tblEventTypes
                    where (EventName == null || EventName == "" || a.Name.ToLower() == EventName.ToLower())
                    orderby a.Name
                    select a);
        }

        public tblEvent SaveEvent(tblEvent _event, List<tblSession> sessions)
        {
            var loc = _db.tblEvents.FirstOrDefault(t => t.Id == _event.Id);
            if (loc == null)
            {
                _event.PasswordKey = Guid.NewGuid().ToString();
                loc = _db.tblEvents.Add(_event);
                _db.SaveChanges();
            }
            else
            {
                loc.Name = _event.Name;
                loc.TotalLength = _event.TotalLength;
                loc.PoolLength = _event.PoolLength;
                loc.Laps = _event.Laps;
                loc.Lanes = _event.Lanes;
                loc.EventDate = _event.EventDate;
                _db.SaveChanges();

            }

            //update already sett ones.
            if (sessions != null)
                foreach (var item in sessions)
                {

                    tblSession dbSession = null;
                    if (item.Id > 0)
                    {
                        dbSession = loc.tblSessions.FirstOrDefault(t => t.Id == item.Id);
                    }
                    if (dbSession == null)
                    {
                        item.PasswordKey = Guid.NewGuid().ToString();
                        loc.tblSessions.Add(item);

                    }
                    else
                    {
                        dbSession.EventId = item.EventId;
                        dbSession.Heat = item.Heat;
                        dbSession.Lanes = item.Lanes;
                        dbSession.TimeFrom = item.TimeFrom;
                        dbSession.TimeTo = item.TimeTo;
                        dbSession.EventId = loc.Id;
                    }
                    _db.SaveChanges();
                }


            return loc;

        }



    }
}
