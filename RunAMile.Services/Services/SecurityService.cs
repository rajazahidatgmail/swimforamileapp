﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RunAMile.Services.Models;


namespace RunAMile.Services
{

    public class SecurityService
    {
        public readonly ILog _log;
        private RunAMileContext _db;
        public SecurityService(  RunAMileContext db)
            : base()
        {
            _log = LogManager.GetLogger(this.GetType());
            _db = db;
            _log.Debug("SecurityService constructor");
        }

        public bool ValidateUser(string username, string password)
        {
            bool result = false;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return result;

            var user = (from u in _db.tblUsers
                        where String.Compare(u.USER_NAME, username, StringComparison.OrdinalIgnoreCase) == 0
                              && String.Compare(u.PASSWORD, password, StringComparison.OrdinalIgnoreCase) == 0
                   
                        select u).FirstOrDefault();

            result = user != null;

            return result;
        }

         

        public tblUser GetUser(string username)
        {
            tblUser user = null;


            user = (from u in _db.tblUsers
                    where String.Compare(u.USER_NAME, username, StringComparison.OrdinalIgnoreCase) == 0
                    select u).FirstOrDefault();

            return user;
        }



        public IQueryable<tblUser> GetUser(string UserName, string FirstName, string Surname)
        {

            UserName = UserName == null ? "" : UserName;
            FirstName = FirstName == null ? "" : FirstName;
            Surname = Surname == null ? "" : Surname;
            IQueryable<tblUser> user = null;

            user = (from u in _db.tblUsers
                    where (UserName == "" || u.USER_NAME.ToLower().Contains(UserName.ToLower()))
                    where (FirstName == "" || u.FIRST_NAME.ToLower().Contains(FirstName.ToLower()))
                    where (Surname == "" || u.SURNAME.ToLower().Contains(Surname.ToLower()))
                    orderby u.USER_NAME
                    select u
                       );

            return user;
        }
        public tblUser GetUser(int? id)
        {
            tblUser user = null;

            user = (from u in _db.tblUsers
                    where u.ID == id
                    select u).FirstOrDefault();

            return user;
        }

        public IQueryable<tblRole> LoadAllRoles()
        {
            IQueryable<tblRole> roles = null;

            roles = (from u in _db.tblRoles
                     orderby u.NAME
                     select u
                       );

            return roles;
        }
        public void SaveUser(tblUser user)
        {
            var existing = GetUser(user.ID);
            if (existing != null)
            {
                existing.ID = user.ID;
                existing.FIRST_NAME = user.FIRST_NAME;
                existing.SURNAME = user.SURNAME;
                existing.USER_NAME = user.USER_NAME;
                if (user.PASSWORD != null)
                    existing.PASSWORD = user.PASSWORD;
                while (existing.tblRoles.Any())
                    existing.tblRoles.Remove(existing.tblRoles.First());

                existing.tblRoles = user.tblRoles;

                _db.SaveChanges();
            }
            else
            {

                _db.tblUsers.Add(user);
                _db.SaveChanges();

            }
        }
        



    }
}
