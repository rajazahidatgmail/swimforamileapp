﻿using log4net;
using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunAMile.Services
{
    public class SessionService
    {
        public readonly ILog _log;
        private RunAMileContext _db;
        public SessionService(RunAMileContext db)
            : base()
        {
            _log = LogManager.GetLogger(this.GetType());
            _db = db;
            _log.Debug("EventService constructor");
        }

        public IQueryable<tblSession> GetSession(Int32? Heat, Int32? id, int EventID)
        {
            return (from a in _db.tblSessions
                    where (Heat.HasValue == false || Heat < 1 || a.Heat == Heat)
                    where (id.HasValue == false || id.Value < 1 || id == a.Id)
                    where (EventID < 1 || EventID == a.EventId)
                    orderby Heat
                    select a);
        }

        public tblSession SaveEvent(tblSession session)
        {
            var loc = _db.tblSessions.FirstOrDefault(t => t.Id == session.Id);
            if (loc == null)
            {
                _db.tblSessions.Add(session);
                _db.SaveChanges();
                return session;
            }
            else
            {
                loc.Heat = session.Heat;
                loc.Lanes = session.Lanes;
                loc.TimeFrom = session.TimeFrom;
                loc.TimeTo = session.TimeTo;
                loc.EventId = session.EventId;

                _db.SaveChanges();
                return loc;
            }
        }
    }
}
