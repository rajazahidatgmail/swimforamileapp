﻿using log4net;
using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunAMile.Services
{
    public class ParticipantService
    {
        public readonly ILog _log;
        private RunAMileContext _db;
        public ParticipantService(RunAMileContext db)
            : base()
        {
            _log = LogManager.GetLogger(this.GetType());
            _db = db;
            _log.Debug("ParticipantService constructor");
        }

        public string ResetEventOfSessionandLane(int SesionId, int LaneNo)
        {
            string result = "";
            try
            {
                var activity = _db.tblSessionActivities.FirstOrDefault(t => t.SessionId == SesionId && t.LaneNo == LaneNo);
                if (activity != null)
                {
                    if (activity.GivenPassword.HasValue && activity.GivenPassword.Value != Guid.Empty) { return "Event has completed and cannot be reset."; }
                    var activitydetails = _db.tblSessionActivityDetails.Where(t => t.SessionActivityId == activity.Id).ToList();
                    foreach (var activitydetail in activitydetails)
                    {
                        var laps = _db.tblLapsDetails.Where(t => t.SessionActivityDetailId == activitydetail.Id);
                        _db.tblLapsDetails.RemoveRange(laps);
                    }
                    _db.tblSessionActivityDetails.RemoveRange(activitydetails);
                    _db.tblSessionActivities.Remove(activity);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }



            return result;
        }
        public IQueryable<tblParticipant> FindParticipants(Int32? EventId = -1, string BIB = "", Int32? sessionId = -1, Int32? Id = -1)
        {
            return (from a in _db.tblParticipants
                    where (EventId <= 0 || a.tblSession.EventId == EventId)

                    where (Id.HasValue == false || Id.Value < 1 || a.Id == Id)
                    where (BIB == null || BIB == "" || a.FName.ToLower().Contains(BIB.ToLower()) || a.LName.ToLower().Contains(BIB.ToLower()) || a.BIB.Contains(BIB))
                    where (sessionId == null || sessionId <= 0 || a.SessionId == sessionId)
                    orderby a.BIB
                    select a);
        }
        public IQueryable<tblParticipant> GetParticipants(Int32? EventId = -1, string name = "", string BIB = "", Int32? sessionId = -1, Int32? Id = -1)
        {
            return (from a in _db.tblParticipants
                    where (EventId <= 0 || a.tblSession.EventId == EventId)

                    where (Id.HasValue == false || Id.Value < 1 || a.Id == Id)
                    where (name == null || name == "" || a.FName.ToLower().Contains(name.ToLower()))
                    where (BIB == null || BIB == "" || a.BIB.Contains(BIB))
                    where (sessionId == null || sessionId <= 0 || a.SessionId == sessionId)
                    orderby a.BIB
                    select a);
        }

        public List<tblParticipant> GetParticipantsOfOtherEvents(Int32? IgnoreThisEventId, List<string> BIPS)
        {
            return (from a in _db.tblParticipants
                    where (a.tblSession.EventId != IgnoreThisEventId)
                    where (BIPS.Contains(a.BIB.ToLower()))
                    select a).ToList();
        }
        public string getEventName(Int32 EventId)
        {
            var _event = (from a in _db.tblEvents
                          where a.Id == EventId
                          select a).FirstOrDefault();
            if (_event != null)
                return _event.Name;
            else
                return "";
        }

        public int DeleteParticipants(int EventId)
        {
            var par = _db.tblParticipants.Where(t => t.tblSession.EventId == EventId).ToList();
            _db.tblParticipants.RemoveRange(par);
            _db.SaveChanges();
            return par.Count;
        }

        public DbContextTransaction BeginTransactions()
        {
            return _db.Database.BeginTransaction();
        }
        public tblParticipant SaveParticipant(tblParticipant participant, bool partialSave = false)
        {
            tblParticipant par = _db.tblParticipants.FirstOrDefault(t => t.Id == participant.Id);

            if (par == null)
            {
                _db.tblParticipants.Add(participant);
                _db.SaveChanges();
                return participant;
            }
            else
            {
                if (partialSave == false)
                {

                    par.LName = participant.LName;
                    par.Age = participant.Age;
                    par.Division = participant.Division;
                    par.Email = participant.Email;
                    par.Gender = participant.Gender;
                    par.SrNo = participant.SrNo;
                    par.StartTime = participant.StartTime;
                }

                par.Lane = participant.Lane;
                par.SessionId = participant.SessionId;
                par.FName = participant.FName;
                par.BIB = participant.BIB;
                _db.SaveChanges();
                return par;
            }
        }

        public string SaveParticipants(List<tblParticipant> participants)
        {
            using (var dbContextTransaction = _db.Database.BeginTransaction())
            {
                try
                {
                    if (participants.Count > 0)
                    {
                        int SessionId = participants[0].SessionId ?? 0;
                        if (SessionId > 0)
                        {
                            var session = _db.tblSessions.FirstOrDefault(t => t.Id == SessionId);
                            if (session != null)
                            {
                                _db.Database.ExecuteSqlCommand("delete from tblParticipants where SessionId in (select Id from tblSession where EventId =  " + session.EventId.ToString() + ")");
                            }
                        }
                    }
                    foreach (var participant in participants)
                    {
                        SaveParticipant(participant);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return "Error:" + ex.Message;
                }
            }
            return "";
        }
        public IQueryable<tblSession> GetSesions(Int32? EventId, Int32? id)
        {
            return (from a in _db.tblSessions
                    where (EventId.HasValue == false || EventId.Value < 1 || EventId == a.EventId)
                    where (id.HasValue == false || id.Value < 1 || id == a.Id)
                    orderby a.Heat
                    select a);
        }

        public bool CheckBIPDuplicity(string BIB, Int32 Id)
        {
            return _db.tblParticipants.Any(t => t.BIB.ToLower() == BIB.ToLower() && t.Id != Id);
        }

        public IQueryable<tblSessionActivity> GetSessionActivity(int? sessionId, int? lanNo)
        {
            return (from a in _db.tblSessionActivities
                    where (sessionId.HasValue == false || sessionId.Value < 1 || a.SessionId == sessionId.Value)
                    where (lanNo.HasValue == false || lanNo.Value < 1 || a.LaneNo == lanNo.Value)
                    select a
                 );
        }
        public IQueryable<tblSessionActivityDetail> GetParticipantsSessionActivityDetail(int? participantId, int? SessionActivityId)
        {
            return (from a in _db.tblSessionActivityDetails
                    where (participantId.HasValue == false || participantId.Value < 1 || a.ParticipantId == participantId.Value)
                    where (SessionActivityId.HasValue == false || SessionActivityId.Value < 1 || a.SessionActivityId == SessionActivityId.Value)

                    select a
                 );
        }

        public IQueryable<tblParticipant> GetPausedParticipantsBySession(int? SessionId)
        {
            return (from a in _db.tblSessionActivities
                    join b in _db.tblSessionActivityDetails on a.Id equals b.SessionActivityId
                    join p in _db.tblParticipants on b.ParticipantId equals p.Id
                    where (SessionId.HasValue == false || SessionId.Value < 1 || a.SessionId == SessionId.Value)
                    where (b.Status == "Paused")
                    select p
                 );
        }
        public IQueryable<tblSessionActivityDetail> GetSessionActivityDetails(int? SessionId)
        {
            return (from a in _db.tblSessionActivityDetails
                    join p in _db.tblParticipants on a.ParticipantId equals p.Id
                    where (SessionId.HasValue == false || SessionId.Value < 1 || a.tblSessionActivity.SessionId == SessionId.Value)
                    where (a.Status == "Paused")
                    select a
                 );
        }
        public IQueryable<tblSessionActivityDetail> GetSessionActivityDetails()
        {
            return (from a in _db.tblSessionActivityDetails
                    select a
                 );
        }
        public IQueryable<tblLapsDetail> GetLapsDetails()
        {
            return (from a in _db.tblLapsDetails
                    select a
                 );
        }
        public string UpdateParticipantLanesAndStatus(List<tblParticipant> Participants, int SessionId, int LaneNo)
        {
            int SessionActivityId = _db.tblSessionActivities.FirstOrDefault(t => t.SessionId == SessionId && t.LaneNo == LaneNo).Id;

            int[] ids = Participants.Select(x => x.Id).ToArray();
            var participants = _db.tblSessionActivityDetails.Where(t => ids.Contains(t.ParticipantId ?? 0) && t.tblSessionActivity.SessionId == SessionId).ToList();
            foreach (var item in participants)
            {
                if (item.tblLapsDetails.Any())
                    item.Status = "started";
                else
                    item.Status = "NotStarted";
                item.SessionActivityId = SessionActivityId;
                item.tblParticipant.Lane = LaneNo;
            }
            _db.SaveChanges();

            return "";
        }

        public IQueryable<tblLapsDetail> GetParticipantsSessionActivityLapsDetail(int? participantId, int? SessionActivityDetailId)
        {
            return (from a in _db.tblLapsDetails
                    join b in _db.tblSessionActivityDetails on a.SessionActivityDetailId equals b.Id
                    where (participantId.HasValue == false || participantId.Value < 1 || b.ParticipantId == participantId.Value)
                    where (SessionActivityDetailId.HasValue == false || SessionActivityDetailId.Value < 1 || a.SessionActivityDetailId == SessionActivityDetailId.Value)
                    select a
                 );
        }

        public string SaveSessionActivity(tblSessionActivity item)
        {

            try
            {


                tblSessionActivity _item = null;
                if (item.Id > 0)
                {
                    _item = GetSessionActivity(item.SessionId, item.LaneNo).FirstOrDefault();
                }

                if (_item == null)//update it.
                {
                    _db.tblSessionActivities.Add(item);
                    _db.SaveChanges();
                }
                else
                {
                    _item.GivenPassword = item.GivenPassword;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _log.Error("services.SaveSessionActivity ", ex);
                return ex.Message;
            }
            return "";
        }
        public string SaveSessionActivityDetail(tblSessionActivityDetail item)
        {

            try
            {
                
                tblSessionActivityDetail _item = GetParticipantsSessionActivityDetail(item.ParticipantId, item.SessionActivityId).FirstOrDefault();

                if (_item == null)//update it.
                {
                    _db.tblSessionActivityDetails.Add(item);
                    _db.SaveChanges();

                }
                else
                {
                    _item.Status = item.Status;
                    //_item.StartTime = item.StartTime;
                    //_item.FinishTime = item.FinishTime;
                    //_item.SessionActivityId = item.SessionActivityId;
                    _db.SaveChanges();
                    item.Id = _item.Id;
                }

            }
            catch (Exception ex)
            {
                _log.Error("services.SaveSessionActivityDetail ", ex);
                return ex.Message;
            }
            return "";
        }

        public string SaveParticipantLapsDetail(List<tblLapsDetail> items, int participantId, int sessionActivityDetailId)
        {

            try
            {
                List<tblLapsDetail> _items = GetParticipantsSessionActivityLapsDetail(participantId, sessionActivityDetailId).ToList();
                foreach (var newitems in items)
                {
                    var alreadyexists = _items.FirstOrDefault(t => t.LapNo == newitems.LapNo);
                    if (alreadyexists == null)
                    {
                        tblLapsDetail newToSave = new tblLapsDetail()
                        {
                            LapStartTime = newitems.LapStartTime,
                            LapNo = newitems.LapNo,
                            LapCompletionTime = newitems.LapCompletionTime,
                            LapFinishTime = newitems.LapFinishTime,
                            SessionActivityDetailId = newitems.SessionActivityDetailId
                        };
                        _db.tblLapsDetails.Add(newToSave);

                    }
                    _db.SaveChanges();


                }



            }
            catch (Exception ex)
            {
                _log.Error("services.SaveParticipantLapsDetail ", ex);
                return ex.Message + " " + (ex.InnerException == null ? "" : ex.InnerException.Message);
            }
            return "";
        }

        public string UpdateEventTime(int p, int sessionId, int LaneNo, string bib, string startOrEnd)
        {

            string error = "failed to update";
            try
            {
                var item = _db.tblSessionActivities.FirstOrDefault(t => t.LaneNo == LaneNo && t.SessionId == sessionId);
                if (item != null)
                {
                    var participant = item.tblSessionActivityDetails.FirstOrDefault(t => t.SessionActivityId == item.Id && t.tblParticipant.BIB == bib);
                    if (participant != null)
                    {
                        if (startOrEnd == "started")//&& participant.StartTime.HasValue == false)
                        {
                            participant.StartTime = DateTime.Now;
                            participant.Status = "started";
                            _db.SaveChanges();
                            error = "";
                        }
                        else if (startOrEnd == "finished")// && participant.FinishTime.HasValue == false)
                        {
                            participant.FinishTime = DateTime.Now;
                            participant.Status = "finished";
                            _db.SaveChanges();
                            error = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                _log.Error(ex);
                throw;
            }
            return error;
        }
    }
}
