﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace RunAMile.Services.Helper
{
    public enum DeviceStatus
    {
        Live = 1,
        Disconnected,
        Trashed
    }

    public class DeviceStatuses
    {
        public static string GetStatus(int id)
        {
            string status = "";
            switch (id.ToString())
            {
                case "1":
                    status = "Live";
                    break;
                case "2":
                    status = "Disconnected";
                    break;
                case "3":
                    status = "Trashed";
                    break;
                default:
                    break;
            }
            return status;

        }

    }

    public static class Helper
    {
        public static string generateMD5(string input)
        {

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));


                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
            }
            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        public static bool VerifyMd5Hash(string input, string hash)
        {
            bool verified = false;
            // Hash the input. 
            string hashOfInput = generateMD5(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                verified = true;
            }

            return verified;

        }

        public static string GetMonthFormattedString(string month, string year)
        {
            return Convert.ToDateTime("1-" + month + "-" + year).ToString("MMM-yyyy");
        }

    }
}