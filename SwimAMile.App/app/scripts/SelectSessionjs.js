﻿
SwimAMile.controller('SelectSessionController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {
    
    $scope.Sessions = [];
    $scope.erroText = '';

    $http.defaults.cache = false;

    $scope.selected = function (e) {
        $scope.itemSelected.Id = e;
        localStorage.setItem("SessionSelectedId", $scope.itemSelected.Id)
        var obj = $filter('filter')($scope.Sessions, { Id: $scope.itemSelected.Id })[0];

        localStorage.setItem("sessionId", obj.Id)
        localStorage.setItem("HeatNo", obj.Heat)
        localStorage.setItem("laneCount", obj.Lanes)
        $scope.erroText = '';
        $scope.selectedSession = 'Heat ' + obj.Heat + ' selected';
        if (obj.TimeFrom != null) {
            var time = new Date(obj.TimeFrom);
            var timetext = time.getUTCHours() + ":" + time.getUTCMinutes() + ":" + time.getUTCSeconds();
            localStorage.setItem("sessionTime", timetext)
        }
    }


    $scope.submit = function () {

        if ($scope.itemSelected != null && $scope.itemSelected.Id > 0) {
            $.mobile.navigate('#SelectLane', transitionEffect);
        }
        else {
            $scope.erroText = 'Select Session';
        }
    }

    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#SELECTSESSION") {
            $scope.itemSelected = {
                Id: 0
            };
            $scope.selectedSession = '';
            var url = apiRoot + 'api/v1/Event/GetSessions/' + localStorage.getItem("password");
            $http.get(url).
                          success(function (data, status, headers, config) {
                              if (status == 200) {

                                  $scope.Sessions = data;


                              }
                          }).
                          error(function (data, status, headers, config) {
                              alert(data);
                          });
        }

    });
    $scope.Back = function () {
        $.mobile.navigate('#Welcome', transitionEffect);
    }

}]);