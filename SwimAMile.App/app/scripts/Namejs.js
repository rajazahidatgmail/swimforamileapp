﻿

SwimAMile.controller('NameController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

    $scope.Sessions = [];

    $http.defaults.cache = false;

    $scope.itemSelected = {};
    $scope.LaneCount = [];
    $scope.selectedLane = 0;
    $scope.ErrorHide = true;
    $scope.Error = '';
    $scope.SessionSelected = function (Count) {
        var sessionId = $scope.itemSelected;
        var singleObj = $filter('filter')($scope.Sessions, { Id: sessionId })[0];
        localStorage.setItem("sessionId", sessionId)
        $scope.LaneCount = [];
        for (var i = 1; i <= singleObj.Lanes; i++) {
            $scope.LaneCount.push(i);
        }
    }

    $scope.submit = function () {

        $scope.Error = '';

        if ($scope.itemSelected == null || $scope.itemSelected < 1) {
            $scope.ErrorHide = false;
            $scope.Error = 'Session is required.';
        }
        if ($scope.selectedLane == null || $scope.selectedLane < 1) {
            $scope.ErrorHide = false;
            $scope.Error = $scope.Error + 'Lane is required.';
        }
        if ($scope.Error == '') {
            localStorage.setItem("laneSelected", $scope.selectedLane)
            $.mobile.navigate('#StartSession', transitionEffect);
        }
    }

    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#NAME") {
            for (var i = 1; i <= 10; i++) {
                $scope.LaneCount.push(i);
            }

            var password = localStorage.getItem("password");
            var url = apiRoot + 'api/v1/Event/GetSessions/' + password;
            $http.get(url).
                          success(function (data, status, headers, config) {
                              if (status == 200) {
                                  $scope.Sessions = data;
                                  //window.localStorage.setItem("sessions", JSON.stringify(data));
                                  $.mobile.navigate('#Welcome', transitionEffect);
                              }
                          }).
                          error(function (data, status, headers, config) {
                              alert(data);
                          });
        }
    });
}]);

