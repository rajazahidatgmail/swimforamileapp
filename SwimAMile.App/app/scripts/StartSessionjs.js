﻿
SwimAMile.controller('StartSessionController', ['$scope', '$http', '$filter', '$timeout', function ($scope, $http, $filter, $timeout) {

    $scope.seconds = 0;//localStorage.getItem("Secondspassed");

    $http.defaults.cache = false;

    $scope.paused = false;
    $scope.disQualified = false;
    $scope.timerOn = false;
    $scope.pauseText = 'Pause';
    $scope.EventStarted = false;
    $scope.ShowFinish = false;

    $scope.waitingfromserver = false;
    $(window).on("navigate", function (e, data) {
        if (location.hash.toUpperCase() == "#STARTSESSION") {
            $scope.waitingfromserver = false;

            $scope.$apply(function () {
                if (localStorage.getItem("Secondspassed") != null && localStorage.getItem("Secondspassed") != "") {
                    $scope.seconds = parseInt(localStorage.getItem("Secondspassed"));
                }

                $scope.participants = JSON.parse(localStorage.getItem("participants"));
                $scope.ShowFinish = false;
                $scope.time = '00:00:00';
                $scope.sessionTime = localStorage.getItem("sessionTime");
                if (localStorage.getItem("EventStarted") == "1") {
                    $scope.EventStarted = true;
                }

                $scope.heat = localStorage.getItem("HeatNo");
                $scope.lane = localStorage.getItem("laneId");
                if ($scope.timerOn == false) {
                    $timeout($scope.tick, 1000);
                    $timeout($scope.SyncLaps, 5000);
                }
                $scope.timerOn = true;
                $scope.calculateTimeFromSeconds();

            });
        }

    });
    $scope.calculateTime = function (startTime, endTime) {
        var totalSeconds = 0;
        if (startTime != null && endTime != null) {


            if (startTime.length > 8)
                startTime = startTime.split('T')[1];
            if (endTime.length > 8)
                endTime = endTime.split('T')[1];



            if (startTime.length == 8)
                startTime = "1/1/2000 " + startTime;
            if (endTime.length == 8)
                endTime = "1/1/2000 " + endTime;

            var duration = new Date(startTime) - new Date(endTime);
            if (duration < 0) {
                duration = duration * -1;
            }

            totalSeconds = $scope.calculateTimeFromSeconds(duration / 1000);

        }
        return totalSeconds;

    }

    $scope.clicked = function (e) {


        var singleObj = $filter('filter')($scope.participants, { id: e })[0];

        if ($scope.disQualified == true) {
            $scope.disQualified = false;
            singleObj.participantStatus = "Disqualified";
            singleObj.statusText = "Disqualified";
            localStorage.setItem("participants", JSON.stringify($scope.participants));
            $scope.save();
            return false;
        }
        if ($scope.paused == true) {
            $scope.paused = false;
            $('.pause').css('opacity', '0.3');
            if (singleObj.participantStatus != 'Disqualified' && singleObj.participantStatus != 'finished') {
                singleObj.participantStatus = "Paused";
                singleObj.statusText = "Paused";
                localStorage.setItem("participants", JSON.stringify($scope.participants));
                $scope.LastPausedObject = singleObj;
                $scope.save(function () {
                    var toRemove = $filter('filter')($scope.participants, { bib: $scope.LastPausedObject.bib })[0];
                    var index = $scope.participants.indexOf(toRemove);
                    $scope.participants.splice(index, 1);

                });
            }
            else {
                return;
            }
        }

        if (singleObj.participantStatus == "NotStarted") {
            singleObj.participantStatus = 'started';
            $scope.SetParticipantEventTime(singleObj.bib, 'started');
            localStorage.setItem('eventStatus', 'started');
            $scope.EventStarted = true;
            localStorage.setItem("EventStarted", "1");
            singleObj.lapStatus = 'started';
            singleObj.startTime = $scope.time;
            if (singleObj.tempLapNo == null)
                singleObj.tempLapNo = 0;

            if (singleObj.tempLapNo > 0)
                singleObj.statusText = (singleObj.tempLapNo - 1) * (singleObj.lapLength * 2);

            if (singleObj.tempLapNo < 1)
                singleObj.tempLapNo = singleObj.tempLapNo + 1;

        }
        else if (singleObj.participantStatus == "started") {
            $scope.EventStarted = true;
            localStorage.setItem("EventStarted", "1");


            if (singleObj.tempLapNo == null)
                singleObj.tempLapNo = 0;
            if (singleObj.laps == null)
                singleObj.laps = [];

            singleObj.itemStatus = 'ns';
            if (singleObj.remainingLaps > 0) {
                singleObj.laps.push({
                    lapStartTime: singleObj.startTime,
                    lapFinishTime: $scope.time,
                    lapNo: singleObj.tempLapNo,
                    bib: singleObj.bib,
                    status: 'ns', //not synced
                    duration: $scope.calculateDurationOfLap(singleObj.startTime, $scope.time)
                });

                singleObj.startTime = $scope.time;
                singleObj.remainingLaps = singleObj.remainingLaps - 1;
                singleObj.length = singleObj.length - singleObj.lapLength;
                singleObj.statusText = (singleObj.tempLapNo) * (singleObj.lapLength * 2);
                if (singleObj.remainingLaps > 0) {

                    singleObj.tempLapNo = singleObj.tempLapNo + 1;
                }
                else {
                    var s = singleObj.laps[0].lapStartTime;
                    var e = singleObj.laps[singleObj.laps.length - 1].lapFinishTime;

                    singleObj.finishTime = $scope.calculateDurationOfLap(s, e);

                    singleObj.participantStatus = 'finished';
                    singleObj.statusText = singleObj.finishTime;

                    $scope.SetParticipantEventTime(singleObj.bib, 'finished');


                }

            }
            else {
                singleObj.itemStatus = 'ns';


                singleObj.participantStatus = 'finished';
                $scope.SetParticipantEventTime(singleObj.bib, 'end');

            }

            var Activeexists = $filter('filter')($scope.participants, { participantStatus: 'started' }).length;
            if (Activeexists == 0) {
                localStorage.setItem("EventStarted", "0");

                $scope.EventStarted = false;
                $scope.ShowFinish = true;
                $scope.save();


            }
        }
        localStorage.setItem("participants", JSON.stringify($scope.participants));
    }
    $scope.Reset = function () {


        var url = apiRoot + 'api/v1/Participant/ResetEvent/' + localStorage.getItem("password") + '/' + localStorage.getItem("sessionId") + '/' + localStorage.getItem("laneId");
        $http.post(url, null).
                      success(function (data, status, headers, config) {
                          if (status == 200) {

                              $scope.EventStarted = false;
                              var ConductedByName = localStorage.getItem("ConductedByName");
                              localStorage.clear();
                              localStorage.setItem("ConductedByName", ConductedByName);
                              $.mobile.navigate("#login", transitionEffect);
                              location.hash = "#login";
                              //$.mobile.changePage("#login", transitionEffect);

                              //window.location.href = "#login";

                          }
                          else {
                              alert('Invalid password for the event.')
                          }
                      }).
                      error(function (data, status, headers, config) {
                          alert(data);
                      });

    }
    $scope.pause = function () {
        $scope.paused = !$scope.paused;
        if ($scope.paused == true) {
            $scope.disQualified = false;
            $('.pause').css('opacity', '1');
            $('.disqualify').css('opacity', '0.3');
            $scope.save();
        }
        else {
            $('.pause').css('opacity', '0.3');

        }

    }
    $scope.import = function () {
        localStorage.setItem("participants", JSON.stringify($scope.participants));
        $scope.save(
            function () {
                $.mobile.navigate('#ImportPaused'); 
             
                            $timeout(function () {
                                var activePage = $.mobile.pageContainer.pagecontainer("getActivePage")[0].id;
                                if ('#' + activePage != location.hash)
                                {
                                    $.mobile.pageContainer.pagecontainer("change", location.hash);
                                }
                                
                            }, 2000);
                        }
            );

    }
    $scope.disqualify = function () {
        $scope.disQualified = !$scope.disQualified;
        if ($scope.disQualified == true) {
            $scope.paused = false;
            $('.disqualify').css('opacity', '1');
            $('.pause').css('opacity', '0.3');
        }
        else {
            $('.disqualify').css('opacity', '0.3');

            $scope.save();
        }
    }
    $scope.tick = function () {
        if ($scope.EventStarted == true) {
            $scope.seconds = $scope.seconds + 1;
            localStorage.setItem("Secondspassed", $scope.seconds)
            $scope.calculateTimeFromSeconds();

        }
        $timeout($scope.tick, 1000);
    }

    $scope.save = function (ExecuteAfterSave) {
        var url = apiRoot + 'api/v1/Participant/SaveActivity/' + localStorage.getItem("password") + '/' + encodeURI(localStorage.getItem("ConductedByName"))

        $http.post(url, $scope.participants).
                      success(function (data, status, headers, config) {
                          if (status == 200) {
                              if (ExecuteAfterSave)
                                  ExecuteAfterSave();
                          }
                          else {
                              alert('Invalid password for the event.')
                          }
                      }).
                      error(function (data, status, headers, config) {
                          alert(data);
                      });


    }
    $scope.finish = function (ExecuteAfterSave) {
        $.mobile.navigate("#Result", transitionEffect);
        location.hash = '#Result';
    }
    $scope.calculateTimeFromSeconds = function () {
        var sec_num = parseInt($scope.seconds);
        if (sec_num == null)
            sec_num = 0;

        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        $scope.time = hours + ':' + minutes + ':' + seconds;

    }
    $scope.calculateDurationOfLap = function (startTime, endTime) {
        if (startTime != null && endTime != null) {


            if (startTime.length > 8)
                startTime = startTime.split('T')[1];
            if (endTime.length > 8)
                endTime = endTime.split('T')[1];



            if (startTime.length == 8)
                startTime = "1/1/2000 " + startTime;
            if (endTime.length == 8)
                endTime = "1/1/2000 " + endTime;

            var duration = new Date(startTime) - new Date(endTime);
            if (duration < 0) {
                duration = duration * -1;
            }

            return $scope.calculateTimeFromSecondsReturn(duration / 1000);

        }

    }
    $scope.calculateTimeFromSecondsReturn = function (seconds) {
        var sec_num = parseInt(seconds);
        if (sec_num == null)
            sec_num = 0;

        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return hours + ':' + minutes + ':' + seconds;

    }
    $scope.SyncLaps = function () {

        $timeout($scope.SyncLaps, 5000);

        if ($scope.waitingfromserver == true) { return; }
        var ToSync = $filter('filter')($scope.participants, { itemStatus: 'ns' }); //not synced participants

        var laps = [];
        angular.forEach(ToSync, function (item) {

            var LapsToSync = $filter('filter')(item.laps, { status: 'ns' }); //not synced laps
            angular.forEach(LapsToSync, function (lap) {

                laps.push(lap);
            });

        });

        if (laps.length < 1) { return; }

        var url = apiRoot + 'api/v1/Participant/SaveLaps/' + localStorage.getItem("sessionId") + '/' + localStorage.getItem("laneId") + '/' + localStorage.getItem("password")
        $http.post(url, laps).
                      success(function (data, status, headers, config) {
                          $scope.waitingfromserver = false;
                          if (status == 200) {
                              //loop through all that were synced to change their statuses
                              angular.forEach(laps, function (item) {

                                  var changeStatus = $filter('filter')($scope.participants, { bib: item.bib })[0]; //not synced
                                  if (changeStatus) {
                                      var lapnos = $filter('filter')(changeStatus.laps, { lapNo: item.lapNo })[0]; //not synced
                                      if (lapnos) {
                                          lapnos.status = 'sd'; //synced
                                      }
                                  }


                              });
                              localStorage.setItem("participants", JSON.stringify($scope.participants));
                          }
                          else {
                              $scope.error = 'Invalid password for the event.';
                          }
                      }).
                      error(function (data, status, headers, config) {
                          $scope.error = data;
                          $scope.waitingfromserver = false;
                      });

        $scope.waitingfromserver = true;


    }
    $scope.SetParticipantEventTime = function (bib, startOrEnd) {

        var url = apiRoot + 'api/v1/Participant/UpdateParticipantTime/' + localStorage.getItem("password") + '/' + localStorage.getItem("sessionId") + '/' + localStorage.getItem("laneId") + '/' + bib + '/' + startOrEnd + '/' + encodeURI(localStorage.getItem("ConductedByName"))
        $http.post(url, null).
        error(function (data, status, headers, config) {
            $scope.error = data;
        });
    }


}]);