﻿SwimAMile.controller('ResultController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

    $scope.tempduration = '';
    $scope.result = [];
    $scope.resultText = {};
    $scope.resultTextVisiblity = false;
    $scope.successText = '';
    $scope.errorText = '';
    $scope.successTextVisibility = false;
    $scope.erroTextTextVisibility = false;

    $http.defaults.cache = false;

    $scope.EvaluateResult = function () {

        $scope.result = [];
        angular.forEach($scope.participants, function (item) {
            if (item.participantStatus != "Disqualified" && item.participantStatus != "Paused") {

                var startTime = '00:00:00';
                var endTime = '00:00:00';
                var seconds = 0;
                if (item.laps != null && item.laps.length > 0) {
                    startTime = item.laps[0].lapStartTime;
                    if (item.laps.length > 1)
                        endTime = item.laps[item.laps.length - 1].lapFinishTime;

                    seconds = $scope.calculateTime(startTime, endTime);
                }
                $scope.$apply(function () {
                    if ($scope.tempduration == null)
                        $scope.tempduration = '-';

                    $scope.result.push({
                        id: item.id,
                        age: item.age,
                        bib: item.bib,
                        division: item.division,
                        email: item.email,
                        fName: item.fName,
                        gender: item.gender,
                        lane: item.lane,
                        lName: item.lName,
                        fullName: item.fName + ' ' + (item.lName == null ? '' : item.lName),
                        sessionId: item.sessionId,
                        srNo: item.srNo,
                        startTime: item.startTime,
                        itemStatus: item.itemStatus, //either modified, not modified
                        participantStatus: item.participantStatus == null || item.participantStatus == "" ? "NotStarted" : item.participantStatus, //either active, disqualified etc
                        remainingLaps: item.remainingLaps,
                        totalLength: item.totalLength,
                        lapLength: item.lapLength,
                        laps: item.laps,
                        lapStatus: '', // either started or finished
                        tempLapNo: item.laps ? item.laps.length : 1,
                        statusText: '', //to be displayed in box
                        seconds: seconds,
                        duration: $scope.tempduration,
                        showLaps: false

                    });
                });
            }
        });


    }
    $scope.showLaps = function (item) {


        var toHide = $filter('filter')($scope.result, { showLaps: true });
        angular.forEach(toHide, function (item1) {
            if (item.bib != item1.bib) {
                item1.showLaps = false;
            }
        });

        item.showLaps = !item.showLaps;

    }
    $scope.ShowResultAsText = function () {
        $scope.resultText = JSON.stringify($scope.result);
        $scope.resultTextVisiblity = true;
        //    $scope.sendEmail();
    }
    $scope.save = function (ExecuteAfterSave) {
        var url = apiRoot + 'api/v1/Participant/SaveActivity/' + localStorage.getItem("password")
        $http.post(url, $scope.participants).
                      success(function (data, status, headers, config) {
                          if (status == 200) {
                              $scope.successText = 'Synchronized successfully.';
                              localStorage.setItem('eventStatus', 'finished');
                              $scope.successTextVisibility = true;
                              $scope.erroTextTextVisibility = false;
                          }
                          else {
                              $scope.errorText = 'Error while saving to server.';
                              $scope.successTextVisibility = false;
                              $scope.erroTextTextVisibility = true;
                          }
                      }).
                      error(function (data, status, headers, config) {
                          $scope.errorText = data;
                          $scope.successTextVisibility = false;
                          $scope.erroTextTextVisibility = true;
                      });


    }
    $scope.BackToLogin = function () {
        $.mobile.navigate('#login', transitionEffect);
    }

    $scope.calculateTime = function (startTime, endTime) {
        var totalSeconds = 0;
        if (startTime != null && endTime != null) {


            if (startTime.length > 8)
                startTime = startTime.split('T')[1];
            if (endTime.length > 8)
                endTime = endTime.split('T')[1];



            if (startTime.length == 8)
                startTime = "1/1/2000 " + startTime;
            if (endTime.length == 8)
                endTime = "1/1/2000 " + endTime;

            var duration = new Date(startTime) - new Date(endTime);
            if (duration < 0) {
                duration = duration * -1;
            }

            totalSeconds = $scope.calculateTimeFromSeconds(duration / 1000);
            $scope.tempduration = totalSeconds;
            totalSeconds = duration;
        }
        return totalSeconds;

    }

    $scope.calculateTimeFromSeconds = function (seconds) {
        var sec_num = parseInt(seconds);
        if (sec_num == null)
            sec_num = 0;

        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return hours + ':' + minutes + ':' + seconds;

    }

    $(window).on("navigate", function (e, data) {
        if (location.hash.toUpperCase() == "#RESULT") {
            localStorage.setItem("eventStatus", "");
            $scope.eventName = JSON.parse(localStorage.getItem("eventSummary")).name;
            $scope.heatNo = localStorage.getItem("HeatNo");
            $scope.laneNo = localStorage.getItem("laneId");
            $scope.participants = JSON.parse(localStorage.getItem("participants"));
            $scope.EvaluateResult();
        }
    });

}]);
