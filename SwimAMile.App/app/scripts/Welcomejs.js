﻿
SwimAMile.controller('WelcomeController', ['$scope', '$http', function ($scope, $http) {

    $scope.name = {};
    $scope.distance = {};
    $scope.length = {};
    $scope.laps = {};
    $http.defaults.cache = false;
    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#WELCOME") {
            $scope.$apply(function () {
                var data = JSON.parse(localStorage.getItem("eventSummary"));

                $scope.name = data.name;
                $scope.distance = data.distance;
                $scope.length = data.length;
                $scope.laps = data.laps;
            });
        }
    });
    
    $scope.submit = function () {
        $.mobile.navigate('#SelectSession', transitionEffect);
    }
    $scope.Back = function () {
        $.mobile.navigate('#login', { transition: transitionEffect.transition, direction: 'reverse' });
    }
}]);