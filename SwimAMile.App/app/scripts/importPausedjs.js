﻿


SwimAMile.controller('ImportPausedController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {


    $scope.participants = [];


    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#IMPORTPAUSED") {
            event.preventDefault();
            event.stopPropagation();
            
            $scope.$apply(function () {
                $scope.sessionId = localStorage.getItem("sessionId")
                $scope.LaneCount = localStorage.getItem("laneId")
                var url = apiRoot + 'api/v1/Participant/GetPausedParticipants/' + $scope.sessionId + '/' + $scope.LaneCount + '/' + localStorage.getItem('password');
                $scope.participants = [];
                $http.defaults.cache = false;

                $http.get(url).
                              success(function (data, status, headers, config) {
                                  if (status == 200) {
                                      angular.forEach(data, function (item) {
                                          if (item.participantStatus != "Disqualified") {

                                              var statusText = '';

                                              if (item.laps.length > 0) {
                                                  statusText = (item.laps.length) * (item.lapLength);
                                              }

                                              $scope.participants.push({
                                                  id: item.id,
                                                  age: item.age,
                                                  bib: item.bib,
                                                  division: item.division,
                                                  email: item.email,
                                                  fName: item.fName,
                                                  gender: item.gender,
                                                  lane: item.lane,
                                                  lName: item.lName,
                                                  sessionId: item.sessionId,
                                                  srNo: item.srNo,
                                                  startTime: item.startTime,
                                                  itemStatus: item.itemStatus, //either modified, not modified
                                                  participantStatus: item.participantStatus, //either active, disqualified etc
                                                  remainingLaps: item.remainingLaps,
                                                  totalLength: item.totalLength,
                                                  lapLength: item.lapLength,
                                                  laps: item.laps,
                                                  lapStatus: statusText, // either started or finished
                                                  tempLapNo: item.laps.length,
                                                  statusText: '' //to be displayed in box
                                                  , checked: false
                                                  , firechange1: function (e) { alert(e); $scope.firechange(e) }

                                              });
                                          }
                                      });
                                  }
                              }).
                              error(function (data, status, headers, config) {
                                  alert(data);
                              });
            });
        }
    });
    $scope.firechange = function (id) { alert(id); }

    $scope.submit = function (id) {

        var participantids = $filter('filter')($scope.participants, { id: id, checked: true });

        if (participantids && participantids.length > 0) {

            var url = apiRoot + 'api/v1/Participant/ImportPausedParticipants/' + $scope.sessionId + '/' + $scope.LaneCount + '/' + localStorage.getItem('password');

            if (participantids.length > 0) {
                $http.post(url, participantids).
                      success(function (data, status, headers, config) {
                          if (status == 200) {
                              $scope.participants = [];
                              angular.forEach(data, function (item) {
                                  if (item.participantStatus != "Disqualified" && item.participantStatus != "Paused") {
                                      var statusText = '';
                                      item.tempLapNo = 1;
                                      if (item.laps.length > 0) {
                                          statusText = (item.laps.length) * (item.lapLength*2);
                                          item.tempLapNo = item.laps.length + 1;
                                          item.startTime = item.laps[item.laps.length-1].lapFinishTime;
                                      }

                                      angular.forEach(item.laps, function (lap) {
                                          if (lap.duration == null) {
                                              lap.duration = $scope.calculateDurationOfLap(lap.lapStartTime, lap.lapFinishTime);
                                          }
                                      });


                                      $scope.participants.push({
                                          id: item.id,
                                          age: item.age,
                                          bib: item.bib,
                                          division: item.division,
                                          email: item.email,
                                          fName: item.fName,
                                          gender: item.gender,
                                          lane: item.lane,
                                          lName: item.lName,
                                          fullName: item.fName + ' ' + (item.lName == null ? '' : item.lName),
                                          sessionId: item.sessionId,
                                          srNo: item.srNo,
                                          startTime: item.startTime,
                                          endTime: '',
                                          itemStatus: item.itemStatus, //either modified, not modified
                                          participantStatus: item.participantStatus == null || item.participantStatus == "" ? "NotStarted" : item.participantStatus, //either active, disqualified etc
                                          remainingLaps: item.remainingLaps,
                                          totalLength: item.totalLength,
                                          lapLength: item.lapLength,
                                          laps: item.laps,
                                          lapStatus: '', // either started or finished
                                          tempLapNo: item.tempLapNo,
                                          statusText: statusText //to be displayed in box
                                      });
                                  }
                              });

                              localStorage.setItem("participants", JSON.stringify($scope.participants));
                              $scope.back();
                          }
                      }).
                      error(function (data, status, headers, config) {
                          alert(data);
                      });
            }
        }
    }
    $scope.back = function () {
        $.mobile.navigate('#StartSession', transitionEffect);

    }


    $scope.calculateDurationOfLap = function (startTime, endTime) {
        if (startTime != null && endTime != null) {


            if (startTime.length > 8)
                startTime = startTime.split('T')[1];
            if (endTime.length > 8)
                endTime = endTime.split('T')[1];



            if (startTime.length == 8)
                startTime = "1/1/2000 " + startTime;
            if (endTime.length == 8)
                endTime = "1/1/2000 " + endTime;

            var duration = new Date(startTime) - new Date(endTime);
            if (duration < 0) {
                duration = duration * -1;
            }

            return $scope.calculateTimeFromSecondsReturn(duration / 1000);

        }

    }
    $scope.calculateTimeFromSecondsReturn = function (seconds) {
        var sec_num = parseInt(seconds);
        if (sec_num == null)
            sec_num = 0;

        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return hours + ':' + minutes + ':' + seconds;

    }

}]);
