﻿
SwimAMile.controller('SelectLaneController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

     
    $scope.erroText = '';
    

    $http.defaults.cache = false;

    $scope.submit = function () {

        if ($scope.itemSelected != null && $scope.itemSelected.Id > 0) {
            localStorage.setItem("laneId", $scope.itemSelected.Id);
            localStorage.setItem("Secondspassed",'0');
            $.mobile.navigate('#StartList', transitionEffect);
        }
        else {
            $scope.erroText = 'Select Lane';
        }
    }
    $scope.selected = function (e) {
        $scope.itemSelected.Id = e;
        $scope.selectedLane = 'Lane ' + e + ' Selected';
        $scope.erroText = '';
    }
    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#SELECTLANE") {
            $scope.itemSelected = {
                Id: 0
            };
            $scope.selectedLane = '';
            $scope.LaneCount = localStorage.getItem("laneCount")
            $scope.sessionId = localStorage.getItem("sessionId")
            $scope.LaneCounts = [];
            $scope.$apply(function () {
                for (var i = 1; i <= $scope.LaneCount; i++) {
                    $scope.LaneCounts.push(i);
                }
            });
        }
    });

    $scope.Back = function () {
        $.mobile.navigate('#SelectSession', transitionEffect);
    }
}]);