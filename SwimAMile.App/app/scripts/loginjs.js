﻿
// create the controller and inject Angular's $scope
SwimAMile.controller('loginController', ['$scope', '$http', function ($scope, $http) {
    // create a message to display in our view
    $scope.message = 'Login Page';
    $scope.uniquID = '';
    $scope.erroText = '';
    $http.defaults.cache = false;
    $scope.canResume = false;


    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#LOGIN") {
            
            if (localStorage.getItem("eventStatus") == 'started') {
                $scope.$apply(function () {
                    $scope.canResume = true;
                });
            }
        }
    });
    $(document).on("pagecontainershow", function (event, ui) {
        var activePage = $.mobile.pageContainer.pagecontainer("getActivePage")[0].id;
        if ('#' + activePage != location.hash) {
            $.mobile.navigate(location.hash, transitionEffect);
        }
    });

    $scope.resume = function () {
        $.mobile.navigate('#StartSession', transitionEffect);
    }

    $scope.validateUniquId = function () {

        if ($scope.uniquID == '') {
            $scope.erroText = 'Enter Unique ID';
            return;
        }
        var url = apiRoot + 'api/v1/Event/GetEventSummary/' + $scope.uniquID;
        $http({
            url: url,
            method: 'GET',
            cache: false,
            headers: {
                'Content-Type': 'application/json'
            }
        }).
                      success(function (data, status, headers, config) {
                          if (status == 200) {
                              gsessions = data;
                              var ConductedByName = localStorage.getItem("ConductedByName");
                              localStorage.clear();
                              localStorage.setItem("ConductedByName", ConductedByName);
                              localStorage.setItem("eventSummary", JSON.stringify(data));
                              localStorage.setItem("password", $scope.uniquID);


                              $.mobile.navigate('#Welcome', transitionEffect);

                          }
                          else {
                              $scope.erroText = data;
                          }
                      }).
                      error(function (data, status, headers, config) {
                          $scope.erroText = data;
                      });

    };
}]);

