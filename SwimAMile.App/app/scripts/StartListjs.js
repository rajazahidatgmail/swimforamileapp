﻿
SwimAMile.controller('startListController', ['$scope', '$http', '$filter', function ($scope, $http, $filter) {

    $scope.participants = [];
    $scope.successmessage = '';


    $scope.errormessage = '';
    $(window).on("navigate", function (event, data) {
        if (location.hash.toUpperCase() == "#STARTLIST") {

            $scope.ConductedByName = '';
            if (localStorage.getItem("ConductedByName") != null && localStorage.getItem("ConductedByName") != "null")
                $scope.ConductedByName = localStorage.getItem("ConductedByName");

            $http.defaults.cache = false;
            $scope.errormessage = '';
            $scope.laneId = localStorage.getItem("laneId");
            $scope.sessionId = localStorage.getItem("sessionId");

            var url = apiRoot + 'api/v1/Participant/GetParticipants/' + $scope.sessionId + '/' + $scope.laneId + '/' + localStorage.getItem('password');
            $http.get(url).
                          success(function (data, status, headers, config) {
                              if (status == 200) {

                                  $scope.participants = [];
                                  angular.forEach(data, function (item) {
                                      if (item.participantStatus != "Disqualified") {
                                          $scope.participants.push({
                                              id: item.id,
                                              age: item.age,
                                              bib: item.bib,
                                              division: item.division,
                                              email: item.email,
                                              fName: item.fName,
                                              gender: item.gender,
                                              lane: item.lane,
                                              lName: item.lName,
                                              fullName: item.fName + ' ' + (item.lName == null ? '' : item.lName),
                                              sessionId: item.sessionId,
                                              srNo: item.srNo,
                                              startTime: item.startTime,
                                              endTime: '',
                                              itemStatus: item.itemStatus, //either modified, not modified
                                              participantStatus: item.participantStatus, //either active, disqualified etc
                                              remainingLaps: item.remainingLaps,
                                              totalLength: item.totalLength,
                                              lapLength: item.lapLength,
                                              laps: item.laps,
                                              lapStatus: '', // either started or finished
                                              tempLapNo: item.laps ? item.laps.length : 1,
                                              statusText: '' //to be displayed in box
                                          });
                                      }
                                  });

                                  localStorage.setItem("participants", JSON.stringify($scope.participants));
                              }
                          }).
                          error(function (data, status, headers, config) {
                              $scope.errormessage = data;
                          });
        }
    });

    $scope.Add = function () {
        if ($scope.errormessage != null && $scope.errormessage.length > 0) { return; }

        var _id = $scope.participants.length * -1;
        $scope.participants.push({ id: _id, bib: '', fullName: '', status: 'changed' });
    }
    $scope.changed = function (e) {
        var singleObj = $filter('filter')($scope.participants, { id: e })[0];
        if (singleObj != null) {
            singleObj.status = 'changed';
            singleObj.lName = '';
            singleObj.fName = singleObj.fullName;

        }
    }
    $scope.SyncOnline = function (executeAfter) {

        if ($scope.errormessage == 'This session and lane has been closed.') {
            return;
        }

        if ($scope.ConductedByName == null || $scope.ConductedByName.length < 2) {
            $scope.errormessage = 'Enter your name.'
            return;
        }
        else {
            localStorage.setItem("ConductedByName", $scope.ConductedByName);
        }

        var tosync = [];
        angular.forEach($scope.participants, function (item) {

            if (item.status == 'changed' && item.bib != "" && item.fullName != "") {
                tosync.push(item);
            }

        });


        $scope.successmessage = '';
        $scope.errormessage = '';
        if (tosync && tosync.length > 0) {
            var url = apiRoot + 'api/v1/Participant/SaveParticipants/' + localStorage.getItem("password") + '/' + $scope.sessionId + '/' + $scope.laneId;
            $http.post(url, tosync).
                        success(function (data, status, headers, config) {
                            if (status == 200) {
                                localStorage.setItem("participants", JSON.stringify(data));

                                $.mobile.navigate('#StartSession', transitionEffect);
                            }
                            else {
                                $scope.errormessage = 'Error while saving.';
                            }
                        }).
                        error(function (data, status, headers, config) {
                            $scope.errormessage = data;
                        });


        }
        else {


            angular.forEach($scope.participants, function (item) {
                if (item.bib != null && item.fullName != null && item.bib.length > 0 && item.fullName.length > 0) {
                    localStorage.setItem("participants", JSON.stringify($scope.participants));
                    $.mobile.navigate('#StartSession', transitionEffect);
                }
            });

        }

    }

    $scope.Back = function () {
        $.mobile.navigate('#SelectLane', transitionEffect);
    }
}]);

