﻿using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwimAMile.App.Models
{
    public class SMEvent
    {
        public SMEvent(tblEvent e)
        {
            Id = e.Id;
            Name = e.Name;
            Length = e.PoolLength ?? 0;
            Distance = e.TotalLength ?? 0;
            Laps = e.Laps ?? 0;

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Distance { get; set; }
        public int Length { get; set; }
        public int Laps { get; set; }
 

    }
}