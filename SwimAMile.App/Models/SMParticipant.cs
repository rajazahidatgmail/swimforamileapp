﻿using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwimAMile.App.Models
{
    public class SMParticipant
    {
        public SMParticipant()
        {
            Laps = new List<Lap>();
        }
        public SMParticipant(tblParticipant p)
        {
            this.Id = p.Id;
            this.Age = p.Age;
            this.BIB = p.BIB;
            this.Division = p.Division;
            this.Email = p.Email;
            this.FName = p.FName;
            this.Gender = p.Gender;
            this.Lane = p.Lane;
            this.LName = p.LName;
            this.SessionId = p.SessionId;
            this.SrNo = p.SrNo;
            this.StartTime = p.StartTime;
      


        }

        public int Id { get; set; }
        public string BIB { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Age { get; set; }
        public string Email { get; set; }
        public string Division { get; set; }
        public Nullable<int> Lane { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> SrNo { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> FinishTime { get; set; }

        public string ItemStatus { get; set; }

        public string ParticipantStatus { get; set; }

        public int RemainingLaps { get; set; }

        public int TotalLength { get; set; }
        public int LapLength { get; set; }

        public List<Lap> Laps { get; set; }
    }

    public class Lap
    {

        public int Id { get; set; }
        public int SessionActivityDetailId { get; set; }
        public DateTime LapStartTime { get; set; }
        public DateTime LapFinishTime { get; set; }
        public int LapNo { get; set; }
        public decimal LapCompletionTime { get; set; }

        public string Bib { get; set; }

    }
}