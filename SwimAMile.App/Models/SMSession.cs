﻿using RunAMile.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwimAMile.App.Models
{
    public class SMSession
    {
        public SMSession()
        {

        }
        public SMSession(tblSession obj)
        {
            this.Id = obj.Id;
            this.EventId = obj.EventId;
            this.Heat = obj.Heat;
            this.TimeFrom = obj.TimeFrom;
            this.TimeTo = obj.TimeTo;
            this.Lanes = obj.Lanes;

        }
        public int Id { get; set; }
        public Nullable<int> EventId { get; set; }
        public int Heat { get; set; }
        public Nullable<System.DateTime> TimeFrom { get; set; }
        public Nullable<System.DateTime> TimeTo { get; set; }
        public int Lanes { get; set; }
    }
}