﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwimAMile.App.Models
{
    public class SMLap
    {


        public DateTime LapStartTime { get; set; }
        public DateTime LapFinishTime{ get; set; }
        public int LapNo { get; set; }
        public string Bib { get; set; }

        
    }
}