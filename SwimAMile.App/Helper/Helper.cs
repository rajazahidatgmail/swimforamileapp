﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
 
using System.Security.Cryptography;
using System.Text;
using RunAMile.Services;
using RunAMile.Services.Models;

namespace SwimAMile.App.Helper
{
    public class Helper
    {

        public static object GetService(Type type)
        {
            var container = new UnityContainer();
            UnityResolver resolver = new UnityResolver(container);
            return resolver.GetService(type);
        }
        public static RequestHelper ValidateRequest(string password)
        {
            EventService service = Helper.GetService(typeof(EventService)) as EventService;
            password = Convert.ToString(password).ToLower().Trim();
            var _event = service.GetEvent("", null).Where(t => t.PasswordKey.ToLower().Trim() == password).FirstOrDefault()   ;
            RequestHelper result = new RequestHelper() { IsValid=false };
            if (_event != null)
            {
                result.IsValid = true;
                result.EventId = _event.Id;
                result.Event = _event;
            }
            return result ;
        }


        public static string generateMD5(string input)
        {
            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
            }
            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }

    public class RequestHelper
    {
        public RequestHelper()
        {
            Event = new tblEvent();
        }
        public bool IsValid { get; set; }
        public int EventId { get; set; }

        public tblEvent Event { get; set; }
        
    }
}