﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SwimAMile.App.Controllers
{
   
    public class HomeController : Controller
    {
        public void Index()
        {
            Response.Redirect("~/index.html");
        }
    }
}
