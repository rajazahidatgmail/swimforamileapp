﻿using log4net;
using Newtonsoft.Json.Linq;
using RunAMile.Services;
using SwimAMile.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace SwimAMile.App.Controllers
{
    public class EventController : ApiController
    {

        EventService _service = null;
        public readonly ILog _log;
        public EventController(EventService service)
        {
            _service = service;
            _log = LogManager.GetLogger(this.GetType());
        }

        [Route("api/v1/Event/GetSessions/{Password}")]
        public HttpResponseMessage GetSessions(string Password)
        {
            List<SMSession> sessions = new List<SMSession>();
            try
            {


                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    sessions = _service.GetSesions(request.EventId, -1)
                        .ToList().Select(t => new SMSession(t)).ToList();
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(sessions).ToString(), Encoding.UTF8, "application/json")
                };


            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        [Route("api/v1/Event/GetEventSummary/{Password}")]
        public HttpResponseMessage GetEventSummary(string Password)
        {
            SMEvent _event = null;
            try
            {
                _log.Info(string.Format(" login request with passsword {0}", Password));

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    _event = _service.GetEvent("", request.EventId)
                        .ToList().Select(t => new SMEvent(t)).FirstOrDefault();

                    _log.Info(string.Format(" login request response with event summary name name={0} distance ={1}  id = {2}", _event.Name, _event.Distance, _event.Id));
                    return Request.CreateResponse(HttpStatusCode.OK, _event);

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid Event");
                }


            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }




    }
}
