﻿using log4net;
using Newtonsoft.Json;
using RunAMile.Services;
using RunAMile.Services.Models;
using SwimAMile.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwimAMile.App.Controllers
{
    public class ParticipantController : ApiController
    {
        ParticipantService _service = null;
        public readonly ILog _log;
        public ParticipantController(ParticipantService service)
        {
            _service = service;
            _log = LogManager.GetLogger(this.GetType());
        }


        [Route("api/v1/Participant/GetParticipants/{SessionId}/{LaneSelected}/{Password}")]
        public HttpResponseMessage GetParticipants(int SessionId, int LaneSelected, string Password)
        {
            List<SMParticipant> ParticipantList = new List<SMParticipant>();
            try
            {
                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    var _sessionActivity = _service.GetSessionActivity(SessionId, LaneSelected).FirstOrDefault();
                    if (_sessionActivity != null && _sessionActivity.GivenPassword.HasValue)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "This session and lane has been closed.");
                    }
                    List<tblSessionActivityDetail> _sessionActivityDetails = new List<tblSessionActivityDetail>();
                    List<tblLapsDetail> _laps = new List<tblLapsDetail>();
                    if (_sessionActivity != null)
                    {
                        _sessionActivityDetails = _service.GetParticipantsSessionActivityDetail(null, _sessionActivity.Id).ToList();

                    }

                    var _partcipants = _service.GetParticipants(-1, "", "", SessionId, -1).Where(t => t.Lane == LaneSelected).ToList();
                    foreach (var p in _partcipants.OrderBy(t => t.BIB))
                    {

                        SMParticipant _p = new SMParticipant(p);

                        string status = "NotStarted";
                        var sessionActivity = _sessionActivityDetails.FirstOrDefault(x => x.ParticipantId == p.Id);
                        if (sessionActivity != null)
                        {
                            status = sessionActivity.Status;
                        }
                        //do not include disqualified participants.
                        if (status.ToLower() == "disqualified")
                        {
                            continue;
                        }
                        _p.ItemStatus = "notchanged";
                        _p.ParticipantStatus = status;

                        _p.RemainingLaps = request.Event.Laps ?? 0;


                        _p.TotalLength = request.Event.TotalLength ?? 0;
                        _p.LapLength = request.Event.PoolLength ?? 0;
                        if (sessionActivity != null && sessionActivity.StartTime.HasValue)
                            _p.StartTime = sessionActivity.StartTime.Value;

                        if (sessionActivity != null && sessionActivity.FinishTime.HasValue)
                            _p.FinishTime = sessionActivity.FinishTime;


                        //_p.Laps = 
                        if (_sessionActivityDetails != null && _sessionActivityDetails.Count > 0)
                        {
                            var sessionActivityDetail = _service.GetParticipantsSessionActivityDetail(p.Id, _sessionActivity.Id).FirstOrDefault();
                            if (sessionActivityDetail != null)
                            {
                                _p.Laps = _service.GetParticipantsSessionActivityLapsDetail(p.Id, sessionActivityDetail.Id).ToList()
                                    .Select(s => new Lap
                                    {
                                        Id = s.Id,
                                        LapStartTime = s.LapStartTime ?? DateTime.MinValue,
                                        LapFinishTime = s.LapFinishTime ?? DateTime.MinValue,
                                        LapNo = s.LapNo ?? 0,
                                        LapCompletionTime = s.LapCompletionTime ?? 0m,
                                        SessionActivityDetailId = s.SessionActivityDetailId ?? 0
                                    }).ToList();
                                _p.RemainingLaps = _p.RemainingLaps - _p.Laps.Count;

                            }
                        }
                        ParticipantList.Add(_p);
                    }
                }

                //make a switch for participants
                ParticipantList = ReArrangeForVerticalColumnFit(ParticipantList);

                return Request.CreateResponse(HttpStatusCode.OK, ParticipantList);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private List<SMParticipant> ReArrangeForVerticalColumnFit(List<SMParticipant> ParticipantList)
        {
            List<SMParticipant> results = new List<SMParticipant>();
            //to make vertical column wise align, participants will be filled like this.
            int count = ParticipantList.Count;
            int midle = count / 2;

            if (count % 2 != 0)
                midle += 1;

            int startA = 0;
            int startB = midle;

            while (startB <= count)
            {
                if (startA < midle)
                    results.Add(ParticipantList[startA]);
                if (startB < count)
                    results.Add(ParticipantList[startB]);

                startB++;
                startA++;
            }

            return results;
        }


        [Route("api/v1/Participant/GetPausedParticipants/{SessionId}/{LaneSelected}/{Password}")]
        public HttpResponseMessage GetPausedParticipants(int SessionId, int LaneSelected, string Password)
        {
            List<SMParticipant> ParticipantList = new List<SMParticipant>();
            try
            {
                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    var _partcipants = _service.GetPausedParticipantsBySession(SessionId).Distinct().ToList();
                    var _sessionActivityDetails = _service.GetSessionActivityDetails(SessionId).Distinct().ToList();

                    foreach (var p in _partcipants)
                    {

                        SMParticipant _p = new SMParticipant(p);

                        string status = "";
                        var sessionActivity = _sessionActivityDetails.FirstOrDefault(x => x.ParticipantId == p.Id);
                        if (sessionActivity != null)
                        {
                            status = sessionActivity.Status;
                        }
                        _p.ItemStatus = "Paused";
                        _p.ParticipantStatus = status;
                        _p.RemainingLaps = request.Event.Laps ?? 0;
                        _p.TotalLength = request.Event.TotalLength ?? 0;
                        _p.LapLength = request.Event.PoolLength ?? 0;
                        //_p.Laps = 
                        if (_sessionActivityDetails.Count > 0)
                        {
                            var sessionActivityDetail = _sessionActivityDetails.Where(t => t.ParticipantId == p.Id).FirstOrDefault();
                            if (sessionActivityDetail != null)
                            {
                                _p.Laps = _service.GetParticipantsSessionActivityLapsDetail(p.Id, sessionActivityDetail.Id).ToList()
                                    .Select(s => new Lap
                                    {
                                        Id = s.Id,
                                        LapStartTime = s.LapStartTime ?? DateTime.MinValue,
                                        LapFinishTime = s.LapFinishTime ?? DateTime.MinValue,
                                        LapNo = s.LapNo ?? 0,
                                        LapCompletionTime = s.LapCompletionTime ?? 0m,
                                        SessionActivityDetailId = s.SessionActivityDetailId ?? 0
                                    }).ToList();
                                _p.RemainingLaps = _p.RemainingLaps - _p.Laps.Count;

                            }
                        }
                        ParticipantList.Add(_p);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, ParticipantList);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/v1/Participant/ImportPausedParticipants/{SessionId}/{LaneSelected}/{Password}")]
        public HttpResponseMessage ImportPausedParticipants(int SessionId, int LaneSelected, List<SMParticipant> participantids, string Password)
        {
            _log.Info(string.Format(" sessionid{0}  lane{1}  participants{2}  paassword{3}", SessionId, LaneSelected, String.Join(",", participantids), Password));
            try
            {
                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {
                    List<tblParticipant> ParticipantList = new List<tblParticipant>();
                    var _sessionActivity = _service.GetSessionActivity(SessionId, LaneSelected).FirstOrDefault();
                    List<tblParticipant> _participants = new List<tblParticipant>();
                    List<tblLapsDetail> _laps = new List<tblLapsDetail>();
                    if (_sessionActivity == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "invalid session or lane.");
                    }

                    int[] ids = participantids.Select(t => t.Id).ToArray();
                    var _partcipants = _service.GetPausedParticipantsBySession(SessionId).Where(t => ids.Contains(t.Id)).ToList();

                    foreach (var p in _partcipants)
                    {
                        ParticipantList.Add(p);
                    }

                    if (ParticipantList.Count > 0)
                    {
                        _service.UpdateParticipantLanesAndStatus(ParticipantList, SessionId, LaneSelected);
                    }

                    return GetParticipants(SessionId, LaneSelected, Password);
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Request is invalid.");
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/v1/Participant/SaveActivity/{Password}/{ConductedByName}")]
        public HttpResponseMessage SaveActivity(List<SMParticipant> participants, string Password, string ConductedByName)
        {

            try
            {

                string result = JsonConvert.SerializeObject(participants);
                _log.Info("SaveActivity JSON received as " + result);
                if (result == null || result.Length < 1)
                {
                    _log.Error("Error while saving activity as nothing passed in to save");
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error while saving activity as nothing passed in to save");
                }

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    if (participants.Count > 0)
                    {
                        var first = participants.FirstOrDefault(t => t.ParticipantStatus.ToLower() == "started" ||
                        t.ParticipantStatus.ToLower() == "finished" ||
                        t.ParticipantStatus.ToLower() == "notstarted" ||
                        t.ParticipantStatus.ToLower() == "disqualified"
                        );

                        if (first == null)
                        {
                            first = participants.FirstOrDefault();
                        }
                        int sessionId = first.SessionId ?? 0;
                        int lanNo = first.Lane ?? 0;
                        int SessionActivityId = -1;
                        //SESSION ACTIVITY HANDLING
                        var sessionActivity = _service.GetSessionActivity(sessionId, lanNo).FirstOrDefault();

                        bool ExistsStarted = participants.Where(t => t.ParticipantStatus.ToLower() == "started").Any();
                        bool ExistsFinished = participants.Where(t => t.ParticipantStatus.ToLower() == "finished").Any();
                        bool ExistsNotStarted = participants.Where(t => t.ParticipantStatus.ToLower() == "notstarted").Any();

                        if (sessionActivity == null) //if activity is null then save it.
                        {
                            tblSessionActivity item = new tblSessionActivity()
                             {
                                 LaneNo = lanNo,
                                 SessionId = sessionId,
                                 Date_Time = DateTime.Now,
                                 ConductedByName = ConductedByName
                             };
                            string message = _service.SaveSessionActivity(item);
                            if (message != "")
                            {
                                _log.Error("Error while adding activity i.e." + message);
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                            SessionActivityId = item.Id;
                        }
                        else
                        {
                            if (ExistsStarted == false && ExistsFinished && ExistsNotStarted == false)
                            {
                                sessionActivity.GivenPassword = Guid.NewGuid();
                                string message = _service.SaveSessionActivity(sessionActivity);
                            }
                            SessionActivityId = sessionActivity.Id;
                        }

                        var paused = _service.GetPausedParticipantsBySession(sessionId);

                        //SESSION DETAIL HANDLING
                        foreach (var item in participants)
                        {
                            //if we allow this to save then it can cause to insert duplicate paused, one for imported to another lane and one for this one.
                            if (item.ParticipantStatus.ToLower() == "paused" && paused.Any(t => t.BIB == item.BIB)) { continue; }//already marked as pause
                            tblSessionActivityDetail activityDetail = new tblSessionActivityDetail()
                            {
                                SessionActivityId = SessionActivityId,
                                ParticipantId = item.Id,
                                Status = item.ParticipantStatus,
                                StartTime = item.StartTime,
                                FinishTime = item.FinishTime
                            };


                            string message = _service.SaveSessionActivityDetail(activityDetail);
                            if (message != "")
                            {
                                _log.Error("Error while adding SaveSessionActivityDetail i.e." + message);
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                            if (item.Laps != null && item.Laps.Count > 0)
                            {
                                List<tblLapsDetail> laps = item.Laps.Select(t =>
                                    new tblLapsDetail
                                    {
                                        Id = t.Id,
                                        LapNo = t.LapNo,
                                        SessionActivityDetailId = activityDetail.Id,
                                        LapStartTime = t.LapStartTime,
                                        LapFinishTime = t.LapFinishTime,
                                        LapCompletionTime = (decimal)t.LapFinishTime.Subtract(t.LapStartTime).TotalSeconds
                                    }).ToList();
                                message = _service.SaveParticipantLapsDetail(laps, item.Id, activityDetail.Id);
                                if (message != "")
                                {
                                    _log.Error("Error while adding SaveParticipantLapsDetail i.e." + message);
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                                }
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "SUCCESS");
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "invalid model state");
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/v1/Participant/SaveLaps/{SessionId}/{LaneSelected}/{Password}")]
        public HttpResponseMessage SaveLaps(List<Lap> Laps, int SessionId, int LaneSelected, string Password)
        {

            try
            {
                string result = JsonConvert.SerializeObject(Laps);
                _log.Info("SaveActivity JSON received as " + result);

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    if (Laps.Count > 0)
                    {

                        var sessionActivity = _service.GetSessionActivity(SessionId, LaneSelected).FirstOrDefault();
                        if (sessionActivity != null)
                        {
                            foreach (var item in Laps)
                            {
                                string bib = item.Bib.ToLower();
                                var sessionActivityDetail = _service.GetSessionActivityDetails().Where(t => t.SessionActivityId == sessionActivity.Id && t.tblParticipant.BIB.ToLower() == bib).FirstOrDefault();
                                if (sessionActivityDetail != null)
                                {
                                    //List<tblLapsDetail> _laps = _service.GetLapsDetails().Where(t => t.SessionActivityDetailId== sessionActivityDetail.Id).ToList();
                                    //bool exists=false;

                                    //if (_laps!=null && _laps.Any(t=>t.LapNo== item.LapNo))
                                    //{
                                    //    exists=true;
                                    //}
                                    //    if(exists==false)
                                    //    {
                                    List<tblLapsDetail> laps =
                                        new List<tblLapsDetail>(){
                                            new tblLapsDetail(){LapNo = item.LapNo,
                                            SessionActivityDetailId = sessionActivityDetail.Id,
                                            LapStartTime = item.LapStartTime,
                                            LapFinishTime = item.LapFinishTime,
                                            LapCompletionTime = (decimal)item.LapFinishTime.Subtract(item.LapStartTime).TotalSeconds
                                            }
                                        };
                                    string message = _service.SaveParticipantLapsDetail(laps, sessionActivityDetail.ParticipantId ?? 0, sessionActivityDetail.Id);
                                    if (message != "")
                                    {
                                        _log.Error("Error while adding laps detail i.e." + message);
                                        return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                                    }
                                }
                            }

                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, "SUCCESS");


            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        [Route("api/v1/Participant/ResetEvent/{Password}/{sessionId}/{lanNo}")]
        public HttpResponseMessage ResetEvent(string Password, int sessionId, int lanNo)
        {

            try
            {

                _log.Info("Reset Event received JSON received as session id is " + sessionId.ToString() + " lane no is " + lanNo.ToString());

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {
                    var session = _service.GetSesions(request.EventId, sessionId).FirstOrDefault();
                    if (session != null && session.Id == sessionId)
                    {
                        string result = _service.ResetEventOfSessionandLane(sessionId, lanNo);
                        if (result == "")
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Invalid session");
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/v1/Participant/UpdateParticipantTime/{Password}/{sessionId}/{lanNo}/{bib}/{startOrEnd}/{ConductedByName}")]
        public HttpResponseMessage UpdateParticipantTime(string Password, int sessionId, int lanNo, string bib, string startOrEnd, string ConductedByName)
        {

            try
            {

                _log.Info("UpdateEventTime JSON received as password is " + Password + " startorend time is " + startOrEnd);

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    if (startOrEnd == "started")
                    {
                        var participant = _service.GetParticipants(request.EventId, "", "", sessionId, -1).FirstOrDefault(t => t.BIB == bib);

                        var response = SaveActivity(new List<SMParticipant>() { new SMParticipant(participant) { ParticipantStatus = startOrEnd } }, Password, ConductedByName);
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            _log.Error(String.Format("Error while saving event details. password{0}  bib{1}", Password, bib));
                        }
                    }

                    string error = _service.UpdateEventTime(request.EventId, sessionId, lanNo, bib, startOrEnd);
                    if (error == "")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "OK");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, error);

                    }
                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Invalid session");
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("api/v1/Participant/SaveParticipants/{Password}/{sessionId}/{lanNo}")]
        public HttpResponseMessage SaveParticipants(List<SMParticipant> participants, string Password, int sessionId, int lanNo)
        {

            try
            {
                string result = JsonConvert.SerializeObject(participants);
                _log.Info("SaveParticipants JSON received as " + participants);

                var request = Helper.Helper.ValidateRequest(Password);
                if (request.IsValid)
                {

                    if (participants.Count > 0)
                    {
                        int SessionActivityId = -1;
                        //SESSION ACTIVITY HANDLING
                        var sessionActivity = _service.GetSessionActivity(sessionId, lanNo).FirstOrDefault();
                        if (sessionActivity == null) //if activity is null then save it.
                        {
                            tblSessionActivity item = new tblSessionActivity()
                            {
                                LaneNo = lanNo,
                                SessionId = sessionId,
                                Date_Time = DateTime.Now
                            };
                            string message = _service.SaveSessionActivity(item);
                            if (message != "")
                            {
                                _log.Error("Error while adding activity i.e." + message);
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, message);
                            }
                            SessionActivityId = item.Id;
                        }
                        else
                        {
                            SessionActivityId = sessionActivity.Id;
                        }

                        //SESSION DETAIL HANDLING
                        using (var transaction = _service.BeginTransactions())
                        {
                            foreach (var item in participants)
                            {
                                if (item.Id < 1)
                                {
                                    var exists = _service.GetParticipants(request.EventId, "", "", null, null).Where(t => t.BIB.ToLower() == item.BIB.ToLower()).FirstOrDefault();
                                    if (exists != null)
                                    {
                                        transaction.Rollback();
                                        return Request.CreateResponse(HttpStatusCode.InternalServerError, item.BIB + " already exists");
                                    }

                                    tblParticipant p = new tblParticipant() { BIB = item.BIB, FName = item.FName, SessionId = sessionId, Lane = lanNo };
                                    _service.SaveParticipant(p);
                                    item.Id = p.Id;
                                }
                                else
                                {
                                    //if for same event, there exists another bib with not matching this id then break now.
                                    var exists = _service.GetParticipants(request.EventId, "", item.BIB, null, null).Where(t => t.Id != item.Id).FirstOrDefault();
                                    if (exists != null)
                                    {
                                        transaction.Rollback();
                                        return Request.CreateResponse(HttpStatusCode.InternalServerError, item.BIB + " already exists");
                                    }

                                    var p = _service.GetParticipants(request.EventId, "", "", sessionId, item.Id).FirstOrDefault();
                                    if (p != null)
                                    {
                                        p.FName = item.FName;
                                        p.LName = "";
                                        p.BIB = item.BIB;
                                        _service.SaveParticipant(p);
                                    }
                                }
                            }
                            transaction.Commit();
                        }


                    }


                    //return Request.CreateResponse(HttpStatusCode.OK, participants);
                    return GetParticipants(sessionId, lanNo, Password);

                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, "invalid model state");
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
